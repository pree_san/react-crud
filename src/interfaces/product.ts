// halaman ini diperlukan agar key dan value dari looping products di halaman Products.tsx bisa di baca
export interface Product{
    id: number;
    title: string;
    image: string;
    likes: number;
}