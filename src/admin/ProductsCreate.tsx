import React, { SyntheticEvent, useState } from 'react'
import { useNavigate } from 'react-router-dom';
import Wrapper from './Wrapper'

const ProductsCreate = () => {
    const navigate = useNavigate();
    const [title, setTitle] = useState("");
    const [image, setImage] = useState("");
    const [redirect, setRedirect] = useState(false);

    const submit = async (e: SyntheticEvent) => {
        e.preventDefault(); // menjaga agar halaman tidak di refresh otomatis
        await fetch("http://localhost:8000/api/products", {
            method: "POST",
            headers: {"Content-Type": "application/json"},
            body: JSON.stringify( {
                title,
                image
            })
        });

        setRedirect(true);
    }

    if(redirect){
        navigate("/admin/products");
    }
    return (
        <Wrapper>
            <form onSubmit={submit}>
                <div className="form-group">
                    <label>TItle</label>
                    <input type="text" className="form-control" name="title"
                        onChange={e => setTitle(e.target.value)}
                    />
                </div>
                <div className="form-group">
                    <label>Image</label>
                    <input type="text" className="form-control" name="image"
                        onChange={e => setImage(e.target.value)}
                    />
                </div>
                <button className="btn btn-outline-secondary mt-3">Save</button>
            </form>
        </Wrapper>
    )
}

export default ProductsCreate;