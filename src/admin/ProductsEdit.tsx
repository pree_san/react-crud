import React, { SyntheticEvent, useEffect, useState } from 'react'
import { useNavigate, useParams } from 'react-router-dom';
import Wrapper from './Wrapper'

// props: PropsWithRef<any> -> ini digunakan di react-router-dom versi lama
const ProductsEdit = () => {
    let { id } = useParams(); // dengan useParamas, id didapatkan dari rout ProductEdit Apps.tsx (..:id). cara baru di react-router-dom v6 
    const navigate = useNavigate();
    const [title, setTitle] = useState("");
    const [image, setImage] = useState("");
    const [redirect, setRedirect] = useState(false);

    useEffect( () => {
        (
            async () => {
                // const response = await fetch(`http://localhost:8000/api/products/${props.match.params.id}`); -> cara pemanggilan id dengan react-router-dom versi lama
                const response = await fetch(`http://localhost:8000/api/products/${id}`);
                const product = await response.json();
                setTitle(product.title);
                setImage(product.image);
            }
        )();
    }, []);

    const submit = async (e: SyntheticEvent) => {
        e.preventDefault(); // menjaga agar halaman tidak di refresh otomatis
        await fetch(`http://localhost:8000/api/products/${id}`, {
            method: "PUT",
            headers: {"Content-Type": "application/json"},
            body: JSON.stringify( {
                title,
                image
            })
        });

        setRedirect(true);
    }

    if(redirect){
        navigate("/admin/products");
    }
    return (
        <Wrapper>
            <form onSubmit={submit}>
                <div className="form-group">
                    <label>TItle</label>
                    <input type="text" className="form-control" name="title"
                        defaultValue={title} onChange={e => setTitle(e.target.value)}
                    />
                </div>
                <div className="form-group">
                    <label>Image</label>
                    <input type="text" className="form-control" name="image"
                        defaultValue={image} onChange={e => setImage(e.target.value)}
                    />
                </div>
                <button className="btn btn-outline-secondary mt-3">Save</button>
            </form>
        </Wrapper>
    )
}

export default ProductsEdit;